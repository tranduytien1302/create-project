﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Models;
namespace Business.Interface
{
    public interface ICustomerBusiness
    {
        IEnumerable<DAL.Models.Customer> GetAll();
        DAL.Models.Customer GetById(int id);
        void Add(DAL.Models.Customer objInfo);
        void Update(DAL.Models.Customer objInfo);
        void Delete(DAL.Models.Customer objInfo);
        void Delete(int id);
    }
}
