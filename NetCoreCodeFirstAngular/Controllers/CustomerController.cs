﻿// ====================================================
// More Templates: https://www.ebenmonney.com/templates
// Email: support@ebenmonney.com
// ====================================================

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using QuickApp.ViewModels;
using AutoMapper;
using Business.Interface;
using Microsoft.AspNetCore.Authorization;

namespace QuickApp.Controllers
{
    //[AllowAnonymous]
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        //private IUnitOfWork _unitOfWork;
        //readonly ILogger _logger;
        //readonly IEmailSender _emailer;
        private ICustomerBusiness _csBusiness;

        public CustomerController(ICustomerBusiness csBusiness)
        {
            _csBusiness = csBusiness;
        }



        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            var rst = _csBusiness.GetAll();
            return Ok(Mapper.Map<IEnumerable<CustomerViewModel>>(rst));
        }



        [HttpGet("throw")]
        public IEnumerable<CustomerViewModel> Throw()
        {
            throw new InvalidOperationException("This is a test exception: " + DateTime.Now);
        }



        [HttpGet("email")]
        public async Task<string> Email()
        {
            //string recepientName = "QickApp Tester"; //         <===== Put the recepient's name here
            //string recepientEmail = "test@ebenmonney.com"; //   <===== Put the recepient's email here

            //string message = EmailTemplates.GetTestEmail(recepientName, DateTime.UtcNow);

            //(bool success, string errorMsg) = await _emailer.SendEmailAsync(recepientName, recepientEmail, "Test Email from QuickApp", message);

            //if (success)
            //    return "Success";

            return "Error: " ;
        }



        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var rst = _csBusiness.GetById(id);
            return Ok(Mapper.Map<CustomerViewModel>(rst));
        }



        // POST api/values
        [HttpPost]
        public void Post([FromBody]CustomerViewModel value)
        {
            var obj = Mapper.Map<DAL.Models.Customer>(value);
            _csBusiness.Add(obj);
        }



        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]CustomerViewModel value)
        {
            var obj = Mapper.Map<DAL.Models.Customer>(value);
            obj.Id = id;
            _csBusiness.Add(obj);
        }



        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _csBusiness.Delete(id);
        }
    }
}
